var URL_SERVER = 'http://pdu-francuchin.c9users.io/';
var app = angular.module('WebApp', [
    'ngRoute',
    'ngAnimate',
    'ngSanitize',
    'ngMaterial',
    'ngMap'
]).config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('grey');
});

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "partials/home.html",
                controller: "ListadoCtrl"
            })
            .when("/lugar/:id", {
                templateUrl: "partials/lugar.html",
                controller: "LugarCtrl"
            })
            .when("/lista/:id", {
                templateUrl: "partials/lista.html",
                controller: "ListaCtrl"
            })
            .when("/404", {
                templateUrl: "partials/404.html",
                controller: "PageCtrl"
            })
            .otherwise({
                redirectTo: "/404"
            });
    }
]);
app.controller('LugarCtrl', function($scope, $routeParams, $http, $location, NgMap, $mdDialog) {
    $scope.selectedIndex = 0;
    this.portada = '/public/media/logo_pdu.png';
    $http.get(URL_SERVER + 'lugar/getLugar/' + $routeParams.id)
        .then(function(response) {
            $scope.lugar = response.data;
            document.getElementById('titulo_app').innerHTML = $scope.lugar.nombre;
        }, function() {
            $location.path('/404');
        });
    $http.get(URL_SERVER + 'lugar/getImagenes/' + $routeParams.id)
        .then(function(response) {
            $scope.images = response.data;
            this.portada = response.data[0].imagen;
        });
    NgMap.getMap().then(function(map) {
        $scope.map = map;
    });
    $scope.centerMap = function() {
        $scope.map.setCenter(new google.maps.LatLng($scope.lugar.latitud, $scope.lugar.longitud));
    };
    $http.get(URL_SERVER + 'lugar/obtenerComentariosConImagen/' + $routeParams.id)
        .then(function(response) {
            $scope.comentarios = response.data;
        });
    $scope.Comentar = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            contentElement: '#myDialog',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        });
    };

    $scope.asigimg = function(img) {
        $scope.imagePath = img;
        $scope.VerImg();
    }
    $scope.VerImg = function(ev) {
        DialogController.img = $scope.imagePath;
        $mdDialog.show({
            controller: ImgDialogController,
            contentElement: '#vista-previa',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        });
    };
});

function ImgDialogController($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}

function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}

app.controller('PageCtrl', function($scope, $location, $http) {

});

app.controller('ListaCtrl', function($scope, $routeParams, $location, $http) {
    $scope.lugares = null;
    $scope.cargando = true;
    $scope.URL_SERVER = URL_SERVER;
    document.getElementById('titulo_app').innerHTML = $routeParams.id;
    $http.get(URL_SERVER + 'lugar/listarLugaresxCategorias/' + $routeParams.id)
        .then(function(response) {
            $scope.lugares = response.data;
            $scope.cargando = false;
        }, function() {
            $location.path('/404');
            $scope.cargando = false;
        });
});

app.controller('ListadoCtrl', function($scope, $location, $http, $location) {
    $scope.cargando = true;
    $scope.URL_SERVER = URL_SERVER;
    document.getElementById('titulo_app').innerHTML = "PaysApp";
    cargarDatos();

    function onError(error) {
        alert('code: ' + error.code + '\n' +
            'message: ' + error.message + '\n');
    }

    function verLocalizacion() {
        navigator.geolocation.getCurrentPosition(function(position) {
            alert('Latitude: ' + position.coords.latitude + '\n' +
                'Longitude: ' + position.coords.longitude + '\n' +
                'Altitude: ' + position.coords.altitude + '\n' +
                'Accuracy: ' + position.coords.accuracy + '\n' +
                'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                'Heading: ' + position.coords.heading + '\n' +
                'Speed: ' + position.coords.speed + '\n' +
                'Timestamp: ' + position.timestamp + '\n');
        }, onError);
    }

    function cargarDatos() {
        $http.get(URL_SERVER + 'lugar/listasCategoriasParaApp')
            .then(function(response) {
                $scope.listas = response.data;
                $scope.cargando = false;
            }, function(response) {
                $location.path('/404');
                $scope.cargando = false;
            });
    }

    $scope.$watch('myPicture', function(value) {
        if (value) {
            $scope.cargando = true;
            $scope.listas = null;
            $http({
                url: URL_SERVER + 'lugar/listasResultadoBusquedaParaApp',
                method: 'POST',
                data: {
                    'imagen': 'data:image/jpeg;base64,' + value
                }
            }).then(
                function(response) {
                    $location.path('/lugar/' + response.data.resultado.id_lugar);
                    $scope.cargando = false;
                },
                function(response) {
                    alert('El servidor esta sufiendo dificultades x_x)');
                    cargarDatos();
                    $scope.cargando = false;
                });
        }
    }, true);
}).directive('camera', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            elm.on('click', function() {
                navigator.camera.getPicture(
                    function(imageURI) {
                        scope.$apply(function() {
                            ctrl.$setViewValue(imageURI);
                        });
                    },
                    function(err) {
                        ctrl.$setValidity('error', false);
                    }, {
                        quality: 100,
                        targetWidth: 512,
                        targetHeight: 512,
                        destinationType: Camera.DestinationType.DATA_URL
                    });
            });
        }
    };
});